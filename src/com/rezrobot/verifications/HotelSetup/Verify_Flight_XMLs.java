package com.rezrobot.verifications.HotelSetup;

import com.rezrobot.flight_circuitry.AirConfig;
import com.rezrobot.flight_circuitry.SearchObject;
import com.rezrobot.flight_circuitry.XMLPriceItinerary;
import com.rezrobot.flight_circuitry.xml_objects.FareRequest;
import com.rezrobot.flight_circuitry.xml_objects.PriceRequest;
import com.rezrobot.flight_circuitry.xml_objects.ReservationRequest;
import com.rezrobot.utill.ReportTemplate;

public class Verify_Flight_XMLs {

	
	public static void verify_LowFareSearchRequest(ReportTemplate PrintTemplate, SearchObject searchObject, AirConfig airConfigObject, FareRequest fareRequest) {
		
		boolean twoway = false;
		if(searchObject.getTriptype().equalsIgnoreCase("Round Trip"))
		{
			twoway = true;
		}
		
		//PrintTemplate.setTableHeading("LOWFARE REQUEST VALIDATIONS");
		String reportfarereqpath = "<a href = ".concat(fareRequest.getUrl()).concat(" target='_blank'>").concat(fareRequest.getUrl()).concat("</a>");
		//PrintTemplate.addToInfoTabel("XML URL", reportfarereqpath);
		
		//Departure date validation-------------------
		String reqorig1date		= "";
		String ReqOrigin1Date	= "";
		if(!fareRequest.getOriginDates1().equalsIgnoreCase("")) {
			reqorig1date = fareRequest.getOriginDates1().trim();
			String[] reqorig1dateARR = reqorig1date.split("-");
			ReqOrigin1Date = reqorig1dateARR[1].concat("/").concat(reqorig1dateARR[2]).concat("/").concat(reqorig1dateARR[0]);		
		} else {
			ReqOrigin1Date = "-";
		}
		PrintTemplate.verifyEqualIgnoreCase(searchObject.getDepartureDate(), ReqOrigin1Date, "Departure Date");
		
		//Return date validation-----------------------
		if(twoway)
		{
			String reqorig2date = "";
			String ReqOrigin2Date = "";
			if(!fareRequest.getOriginDates2().equalsIgnoreCase("")) {
				reqorig2date = fareRequest.getOriginDates2().trim();
				String[] reqorig2dateARR = reqorig2date.split("-");
				ReqOrigin2Date = reqorig2dateARR[1].concat("/").concat(reqorig2dateARR[2]).concat("/").concat(reqorig2dateARR[0]);
			} else {
				ReqOrigin2Date = "-";
			}
			
			PrintTemplate.verifyEqualIgnoreCase(searchObject.getReturnDate(), ReqOrigin2Date, "Arrival Date");
		}
		
		//Leave from - to location validation
		String SobjOrigin1 = "";
		try {
			SobjOrigin1 = searchObject.getFrom().split("\\|")[1];
		} catch (Exception e) {
			SobjOrigin1 = e.getMessage();
		}
		PrintTemplate.verifyEqualIgnoreCase(SobjOrigin1, fareRequest.getOrigin1().trim(), "Leave From");
		String SobjDepart1 = "";
		try {
			SobjDepart1 = searchObject.getTo().split("\\|")[1];
		} catch (Exception e) {
			SobjDepart1 = e.getMessage();
		}
		PrintTemplate.verifyEqualIgnoreCase(SobjDepart1, fareRequest.getDestination1().trim(), "Leave To");
		
		//Return from - to location validation
		if (twoway) {
			String SobjOrigin2 = "";
			String SobjDepart2 = "";
			try {
				SobjOrigin2 = searchObject.getTo().split("\\|")[1];
			} catch (Exception e) {
				SobjOrigin2 = e.getMessage();
			}
			try {
				SobjDepart2 = searchObject.getFrom().split("\\|")[1];
			} catch (Exception e) {
				SobjDepart2 = e.getMessage();
			}
			PrintTemplate.verifyEqualIgnoreCase(SobjOrigin2, fareRequest.getOrigin2().trim(), "Return From");
			PrintTemplate.verifyEqualIgnoreCase(SobjDepart2, fareRequest.getDestination2().trim(), "Return To");
		}
		
		
		PrintTemplate.verifyEqualIgnoreCase(searchObject.getCabinClass(), fareRequest.getFareclass().trim(), "Cabin/Fare class");
		
		String seats = "";
		try {
			seats = String.valueOf(Integer.parseInt(searchObject.getAdult()) + Integer.parseInt(searchObject.getChildren()));
		} catch (NumberFormatException e) {
			seats = e.getMessage();
		}
		PrintTemplate.verifyEqualIgnoreCase(seats, fareRequest.getSeatsRequired().trim(), "No. of Seats required");
		
		if(!searchObject.getPreferredAirline().equals("NONE"))
		{
			PrintTemplate.verifyEqualIgnoreCase(searchObject.getPreferredAirline(), fareRequest.getPrefFlight().trim(), "Preferred Airline Booking");
		}
		
		String Infant   = "";
		String Adults   = "";
		String Children = "";
		Adults			= fareRequest.getAirTravelers().get("ADT");
		Children		= fareRequest.getAirTravelers().get("CHD");
		Infant			= fareRequest.getAirTravelers().get("INF");
		PrintTemplate.verifyEqualIgnoreCase(searchObject.getAdult(), Adults.trim(), "No. of Adult Passengers");
		if(!searchObject.getChildren().equalsIgnoreCase("0"))
		{
			PrintTemplate.verifyEqualIgnoreCase(searchObject.getChildren(), Children.trim(), "No. of Children");
		}
		else
		{
			PrintTemplate.verifyEqualIgnoreCase(searchObject.getChildren(), "0", "No. of Children");
		}
		if(!searchObject.getInfant().equalsIgnoreCase("0"))
		{
			PrintTemplate.verifyEqualIgnoreCase(searchObject.getInfant(), Infant.trim(), "No. of Infants");
		}
		else
		{
			PrintTemplate.verifyEqualIgnoreCase(searchObject.getInfant(), "0", "No. of Infants");
		}
		
		String pricingSource = "";
		if(airConfigObject.getFTypeConfgShopCart().equalsIgnoreCase("Published Fares Only"))
		{
			pricingSource = "Published";
		}
		else if(airConfigObject.getFTypeConfgShopCart().equalsIgnoreCase("Private Fares and if not available then Published Fares "))
		{
			pricingSource = "Both";
		}
		String negoCode = "";
		if(pricingSource.equalsIgnoreCase("Both"))
		{
			negoCode = "SWH10";
		}
		else
		{
			negoCode = "Not Applicable";
		}
		
		PrintTemplate.verifyEqualIgnoreCase(pricingSource, fareRequest.getPricingSource(), "PricingSource Validation");
		PrintTemplate.verifyEqualIgnoreCase(negoCode, fareRequest.getNegoCode().trim(), "Nego Code Validation");
		
		//PrintTemplate.markTableEnd();
	}
	
	public static void verify_PriceRequest(ReportTemplate PrintTemplate, SearchObject searchObject, AirConfig airConfigObject, PriceRequest priceRequest, XMLPriceItinerary XMLSelectFlight) {
	
		//PrintTemplate.setTableHeading("PRICE REQUEST VALIDATIONS");
		String reportfarereqpath = "<a href = ".concat(priceRequest.getUrl()).concat(" target='_blank'>").concat(priceRequest.getUrl()).concat("</a>");
		//PrintTemplate.addToInfoTabel("XML URL", reportfarereqpath);
		
		String pricingSource = "";
		if(airConfigObject.getFTypeConfgShopCart().equalsIgnoreCase("Published Fares Only"))
		{
			pricingSource = "Published";
		}
		else if(airConfigObject.getFTypeConfgShopCart().equalsIgnoreCase("Private Fares and if not available then Published Fares "))
		{
			pricingSource = "Both";
		}
		PrintTemplate.verifyEqualIgnoreCase(XMLSelectFlight.getPricinginfo().getPricingSource().trim(), priceRequest.getPricingSource(), "PricingSource Validation");
		
		String negoCode = "";
		if(XMLSelectFlight.getPricinginfo().getPricingSource().trim().equalsIgnoreCase("Private"))
		{
			negoCode = "SWH10";
			PrintTemplate.verifyEqualIgnoreCase(negoCode, priceRequest.getNegoCode().trim(), "Nego Code Validation");
		}
		else
		{
			negoCode = "";
			PrintTemplate.verifyEqualIgnoreCase(negoCode, priceRequest.getNegoCode().trim(), "Nego Code Validation");
		}
		
		
		
		//PrintTemplate.markTableEnd();
	}
	
	public static void verify_PriceResponse() {
		
		
	}
	
	public static void verify_ReservationRequest(ReportTemplate PrintTemplate, SearchObject searchObject, AirConfig airConfigObject, ReservationRequest reservationRequest, XMLPriceItinerary XMLSelectFlight) {
		
		//PrintTemplate.setTableHeading("RESERVATION REQUEST VALIDATIONS");
		String reportfarereqpath = "<a href = ".concat(reservationRequest.getUrl()).concat(" target='_blank'>").concat(reservationRequest.getUrl()).concat("</a>");
		//PrintTemplate.addToInfoTabel("XML URL", reportfarereqpath);

		String pricingSource = "";
		if(airConfigObject.getFTypeConfgShopCart().equalsIgnoreCase("Published Fares Only"))
		{
			pricingSource = "Published";
		}
		else if(airConfigObject.getFTypeConfgShopCart().equalsIgnoreCase("Private Fares and if not available then Published Fares "))
		{
			pricingSource = "Both";
		}
		PrintTemplate.verifyEqualIgnoreCase(XMLSelectFlight.getPricinginfo().getPricingSource().trim(), reservationRequest.getPriceType(), "PricingSource Validation");
		
		String negoCode = "";
		if(XMLSelectFlight.getPricinginfo().getPricingSource().trim().equalsIgnoreCase("Private"))
		{
			negoCode = "SWH10";
			PrintTemplate.verifyEqualIgnoreCase(negoCode, reservationRequest.getNegoCode().trim(), "Nego Code Validation");
		}
		else
		{
			negoCode = "";
			PrintTemplate.verifyEqualIgnoreCase(negoCode, reservationRequest.getNegoCode().trim(), "Nego Code Validation");
		}
		
		
		//PrintTemplate.markTableEnd();
	}
	
	public static void verify_ReservationResponse() {
		
		
	}
	
	public static void verify_PNRRequest() {
		
		
	}
	
	public static void verify_PNRResponse() {
		
		
	}
	
	public static void verify_EticketRequest() {
		
		
	}

	public static void verify_EticketResponse() {
		
		
	}
	
	public static void verify_PNRUpdateRequest() {
		
		
	}
	
	public static void verify_PNRUpdateResponse() {
		
		
	}
	
	public static void verify_RulesRequest() {
		
		
	}
	
	public static void verify_RulesResponse() {
		
		
	}
	
	public static void verify_CancellationRequest() {
		
		
	}
	
	public static void verify_CancellationResponse() {
		
		
	}
	
	
}
