package com.rezrobot.verifications.HotelSetup;

import java.util.HashMap;
import java.util.Map;

import com.relevantcodes.extentreports.ExtentReports;
import com.rezrobot.flight_circuitry.AirConfig;
import com.rezrobot.flight_circuitry.FlightReservation;
import com.rezrobot.flight_circuitry.SearchObject;
import com.rezrobot.utill.ExtentReportTemplate;
import com.rezrobot.utill.ReportTemplate;

public class Verify_Print_TestData {

	public static void printExcelData(ReportTemplate PrintTemplate, FlightReservation reservation)
	{
		
		SearchObject searchObject = reservation.getSearchObject(); 
		//PRINT TEST INPUT DETAILS
		
		Map<String, String> searchData = new HashMap<String, String>();
		//ExtentReportTemplate report = ExtentReportTemplate.getInstance();
		//PrintTemplate.setInfoTableHeading("Reservation Details - Excel Test data");
		//PrintTemplate.addToInfoTabel("Test ID", searchObject.getScenario());
		searchData.put("Test ID", searchObject.getScenario());
		//PrintTemplate.addToInfoTabel("Execute", String.valueOf(searchObject.getExcecuteStatus()));
		searchData.put("Test ID", searchObject.getScenario());
		//PrintTemplate.addToInfoTabel("Booking Channel(Web/Call Center)", searchObject.getbookingChannel());
		searchData.put("Booking Channel(Web/Call Center)", searchObject.getbookingChannel());
		//PrintTemplate.addToInfoTabel("Is TO booking ? (Yes/No)", searchObject.getTOBooking());
		searchData.put("Is TO booking ? (Yes/No)", searchObject.getTOBooking());
		//PrintTemplate.addToInfoTabel("Quotation Booking", String.valueOf(searchObject.isQuotation()));
		searchData.put("Quotation Booking", String.valueOf(searchObject.isQuotation()));
		//PrintTemplate.addToInfoTabel("Change Air COnfigurations", String.valueOf(searchObject.getAirConfigurationStatus()));
		searchData.put("Change Air COnfigurations", String.valueOf(searchObject.getAirConfigurationStatus()));
		//PrintTemplate.addToInfoTabel("Payment Method(Online/Offline/Flocash)", searchObject.getPaymentMode().toString());
		searchData.put("Payment Method(Online/Offline/Flocash)", searchObject.getPaymentMode().toString());
		//PrintTemplate.addToInfoTabel("Departure Date", searchObject.getDepartureDate());
		searchData.put("Departure Date", searchObject.getDepartureDate());
		//PrintTemplate.addToInfoTabel("Departure Time", searchObject.getDepartureTime());
		searchData.put("Departure Time", searchObject.getDepartureTime());
		//PrintTemplate.addToInfoTabel("Return Date", searchObject.getReturnDate());
		searchData.put("Return Date", searchObject.getReturnDate());
		//PrintTemplate.addToInfoTabel("Return Time", searchObject.getReturnTime());
		searchData.put("Return Time", searchObject.getReturnTime());
		//PrintTemplate.addToInfoTabel("Profit Type (Value/Percentage)", searchObject.getProfitType());
		searchData.put("Profit Type (Value/Percentage)", searchObject.getProfitType());
		//PrintTemplate.addToInfoTabel("Profit Value", String.valueOf(searchObject.getProfit()));
		searchData.put("Profit Value", searchObject.getScenario());
		//PrintTemplate.addToInfoTabel("Booking Fee type (Value/Percentage)", searchObject.getBookingFeeType());
		searchData.put("Booking Fee type (Value/Percentage)", searchObject.getBookingFeeType());
		//PrintTemplate.addToInfoTabel("Booking Fee", String.valueOf(searchObject.getBookingFee()));
		searchData.put("Booking Fee", String.valueOf(searchObject.getBookingFee()));
		//PrintTemplate.addToInfoTabel("Country", searchObject.getCountry());
		searchData.put("Country", searchObject.getCountry());
		//PrintTemplate.addToInfoTabel("Selling Currency", searchObject.getSellingCurrency());
		searchData.put("Selling Currency", searchObject.getSellingCurrency());
		//PrintTemplate.addToInfoTabel("Trip Type (Round Trip/One way/Multi-Destination)", searchObject.getTriptype());
		searchData.put("Trip Type (Round Trip/One way/Multi-Destination)", searchObject.getTriptype());
		//PrintTemplate.addToInfoTabel("Flexible Flights", String.valueOf(searchObject.isFlexible()));
		searchData.put("Flexible Flights", String.valueOf(searchObject.isFlexible()));
		//PrintTemplate.addToInfoTabel("From", searchObject.getFrom());
		searchData.put("From", searchObject.getFrom());
		//PrintTemplate.addToInfoTabel("To", searchObject.getTo());
		searchData.put("To", searchObject.getTo());
		//PrintTemplate.addToInfoTabel("Adult count", searchObject.getAdult());
		searchData.put("Adult count", searchObject.getAdult());
		//PrintTemplate.addToInfoTabel("Children count", searchObject.getChildren());
		searchData.put("Children count", searchObject.getChildren());
		String childrenAge = "";
		for(int y=0; y<searchObject.getChildrenAge().size(); y++){
			childrenAge = childrenAge.concat(searchObject.getChildrenAge().get(y)).concat("/");
		}
		//PrintTemplate.addToInfoTabel("Children Age", childrenAge);
		searchData.put("Children Age", childrenAge);
		//PrintTemplate.addToInfoTabel("Infant Count", searchObject.getInfant());
		searchData.put("Infant Count", searchObject.getInfant());
		//PrintTemplate.addToInfoTabel("Cabin Class", searchObject.getCabinClass());
		searchData.put("Cabin Class", searchObject.getCabinClass());
		//PrintTemplate.addToInfoTabel("Preferred Currency", searchObject.getPreferredCurrency());
		searchData.put("Preferred Currency", searchObject.getPreferredCurrency());
		//PrintTemplate.addToInfoTabel("Preferred Airline", searchObject.getPreferredAirline());
		searchData.put("Preferred Airline", searchObject.getPreferredAirline());
		//PrintTemplate.addToInfoTabel("Request Non-stop Flights", String.valueOf(searchObject.isNonStop()));
		searchData.put("Request Non-stop Flights", String.valueOf(searchObject.isNonStop()));
		//PrintTemplate.addToInfoTabel("Apply Discount - BEC", String.valueOf(searchObject.isApplyDiscount()));
		searchData.put("Apply Discount - BEC", String.valueOf(searchObject.isApplyDiscount()));
		//PrintTemplate.addToInfoTabel("Apply Discount - Search Again (Results Page) ", String.valueOf(searchObject.isDiscountAtSearchAgain()));
		searchData.put("Apply Discount - Search Again (Results Page) ", String.valueOf(searchObject.isDiscountAtSearchAgain()));
		//PrintTemplate.addToInfoTabel("Slecting Flight", searchObject.getSelectingFlight());
		searchData.put("Slecting Flight", searchObject.getSelectingFlight());
		//PrintTemplate.addToInfoTabel("Test Cart Remove Functionality", String.valueOf(searchObject.isRemovecartStatus()));
		searchData.put("Test Cart Remove Functionality", String.valueOf(searchObject.isRemovecartStatus()));
		//PrintTemplate.addToInfoTabel("Test Filter Functionality", String.valueOf(searchObject.isValidateFilters()));
		searchData.put("Test Filter Functionality", String.valueOf(searchObject.isValidateFilters()));
		//PrintTemplate.addToInfoTabel("Apply Discount - Payment Page", String.valueOf(searchObject.isApplyDiscountAtPayPg()));
		searchData.put("Apply Discount - Payment Page", String.valueOf(searchObject.isApplyDiscountAtPayPg()));
		
		//PrintTemplate.addToInfoTabel("Test Cancellation Flow", searchObject.getCancellationStatus());
		searchData.put("Test Cancellation Flow", searchObject.getCancellationStatus());
		//PrintTemplate.addToInfoTabel("Pay from Supplier Payable", String.valueOf(searchObject.getSupplierPayablePayStatus()));
		searchData.put("Pay from Supplier Payable", String.valueOf(searchObject.getSupplierPayablePayStatus()));
		//PrintTemplate.markTableEnd();
		
		PrintTemplate.setInfoTable("Reservation Details - Excel Test data", searchData);
	}
	
	public static void printAirConfigurationData(ReportTemplate PrintTemplate, SearchObject searchObject)
	{
		//PRINT TEST INPUT DETAILS
		Map<String, String> searchData = new HashMap<String, String>();
		ExtentReportTemplate report = ExtentReportTemplate.getInstance();
		
		AirConfig config = new AirConfig();
		config = searchObject.getAirConfigObject();
		
		//PrintTemplate.setInfoTableHeading("Reservation Details - Air Configuration data");
		
		//PrintTemplate.addToInfoTabel("Published Fare Type", config.getPubFareType().toString());
		searchData.put("Published Fare Type", config.getPubFareType().toString());
		//PrintTemplate.addToInfoTabel("Published Fare Booking Fee", config.getPubBookingFee());
		searchData.put("Published Fare Booking Fee", config.getPubBookingFee());
		//PrintTemplate.addToInfoTabel("Published Fare Profit Markup", config.getPubProfitMarkup());
		searchData.put("Published Fare Profit Markup", config.getPubProfitMarkup());
		//PrintTemplate.addToInfoTabel("Published Fare Both", config.getPubBoth());
		searchData.put("Published Fare Both", config.getPubBoth());
		//PrintTemplate.addToInfoTabel("Published Fare None", config.getPubNone());
		searchData.put("Published Fare None", config.getPubNone());
		//PrintTemplate.addToInfoTabel("Private Fare Type", config.getPvtFareType().toString());
		searchData.put("Private Fare Type", config.getPvtFareType().toString());
		//PrintTemplate.addToInfoTabel("Private Fare Booking Fee", config.getPvtBookingFee());
		searchData.put("Private Fare Booking Fee", config.getPvtBookingFee());
		//PrintTemplate.addToInfoTabel("Private Fare Profit Markup", config.getPvtProfitMarkup());
		searchData.put("Private Fare Profit Markup", config.getPvtProfitMarkup());
		//PrintTemplate.addToInfoTabel("Private Fare Both", config.getPvtBoth());
		searchData.put("Private Fare Both", config.getPvtBoth());
		//PrintTemplate.addToInfoTabel("Private Fare None", config.getPvtNone());
		
		//PrintTemplate.addToInfoTabel("Faretype Configuration - Shopping Cart", config.getFTypeConfgShopCart());
		searchData.put("Faretype Configuration - Shopping Cart", config.getFTypeConfgShopCart());
		//PrintTemplate.addToInfoTabel("Faretype Configuration - F+H", config.getFTypeConfgFplusH());
		searchData.put("Faretype Configuration - F+H", config.getFTypeConfgFplusH());
		//PrintTemplate.addToInfoTabel("Faretype Configuration - FP", config.getFTypeConfgFixPack());
		searchData.put("Faretype Configuration - FP", config.getFTypeConfgFixPack());
		
		//PrintTemplate.addToInfoTabel("Flight Payment Option - Cart Booking", config.getFlightPayOptCartBooking());
		searchData.put("Flight Payment Option - Cart Booking", config.getFlightPayOptCartBooking());
		//PrintTemplate.addToInfoTabel("Flight Payment Option - Pay Full F+H", config.getFlightPayOptPayfullFplusH());
		searchData.put("Flight Payment Option - Pay Full F+H", config.getFlightPayOptPayfullFplusH());
		
		//PrintTemplate.markTableEnd();
		
		PrintTemplate.setInfoTable("Reservation Details - Air Configuration data", searchData);
		
	}
	
}
