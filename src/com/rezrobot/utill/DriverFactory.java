package com.rezrobot.utill;

import java.io.File;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.apache.log4j.Logger;


public class DriverFactory {
	
	    private Logger logger = Logger.getLogger(this.getClass()) ;
	    private WebDriver driver;
	    private static DriverFactory myObj = null;
	    private String browser = "firefox";
	 //   private String version = "firefox";
	    private String URL = "";
	    private ScreenShot screenShotCreator;

	    private DriverFactory() {

	  }

	    public void init(String browser, String URL) {
	        this.browser = browser;
	        this.URL = URL;
	       // this.screenShotCreator = new ScreenShot();
	    }

	    public WebDriver getDriver() {
	    	
	        return this.driver;
	    }

	    private void setDriver(WebDriver driver) {
	        this.driver = driver;
	    }

	    public static DriverFactory getInstance() {
	        if(myObj == null) {
	            myObj = new DriverFactory();
	            return myObj;
	        } else {
	            return myObj;
	        }
	    }

	    public void initializeDriver() throws Exception {

			logger.info("====== Driver initialization started ========");
			String Type = PropertySingleton.getInstance().getProperty("BinaryType").trim();

			if (Type.equalsIgnoreCase("FirefoxBin")) {
				logger.info("FirefoxBin type selected ---> No profile");
				FirefoxProfile prof = new FirefoxProfile();
				FirefoxBinary bin = new FirefoxBinary();
				this.setDriver(new FirefoxDriver(bin, prof));
			
			}else if (Type.equalsIgnoreCase("Phantom")) {
				logger.info("Phantom Driver type selected ---> No profile");
				String ExecutablePath = PropertySingleton.getInstance().getProperty("phantomjs.executable.path");
				logger.info("PhantomJs Path --> " + ExecutablePath);
				DesiredCapabilities cap = new DesiredCapabilities();
				cap.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, ExecutablePath);
				cap.setCapability("TakeScreenshot", true);
				this.setDriver(new PhantomJSDriver(cap));
			
			}else if (Type.equalsIgnoreCase("Firefoxpro")) {

				/*System.setProperty("webdriver.gecko.driver", "..//geckodriver.exe");
				WebDriver driver = new FirefoxDriver();
				driver.get("http://www.toolsqa.com");*/
				
				File profile = new File(PropertySingleton.getInstance().getProperty("firefox.profile.path").trim());
				logger.info("Firefox Profile type selected --->Profile --->" + profile.getAbsolutePath());
				FirefoxProfile prof = new FirefoxProfile(profile);
				this.setDriver(new FirefoxDriver(prof));
		
		    }else if (Type.equalsIgnoreCase("Chrome")) {

		         String chromeFilePath = System.getProperty("user.dir") + File.separator + "target" + File.separator + "chromedriver.exe";
		    	 System.out.println(chromeFilePath);
	             System.setProperty("webdriver.chrome.driver", chromeFilePath);

	             this.setDriver(new ChromeDriver());
	
		    }else if (Type.equalsIgnoreCase("IE")) {

		    	String  ieFilePath = System.getProperty("user.dir") + File.separator + "target" + File.separator + "IEDriverServer.exe";
	            System.setProperty("webdriver.ie.driver", ieFilePath);
	            DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
	            caps.setCapability("ignoreZoomSetting", true);
	            caps.setCapability("nativeEvents", false);
	            this.setDriver(new InternetExplorerDriver(caps));
			//	driver.manage().timeouts().implicitlyWait(Long.parseLong(PropertySingleton.getInstance().getProperty("Driver.Timeout")), TimeUnit.SECONDS);

		

			} else {
				FirefoxProfile prof = new FirefoxProfile();
				logger.info("Driver Type Not Selected");
				driver = new FirefoxDriver(prof);
				
		    }

		
			driver.manage().timeouts().implicitlyWait(Long.parseLong(PropertySingleton.getInstance().getProperty("Driver.Timeout")), TimeUnit.SECONDS);
			this.getDriver().manage().window().maximize();
	    	

	    }
	    
	    
	    public void initializeRemoteDriver() {
	    	
	    	
	    }

	    public void navigateToURL() {
	        this.getDriver().get(this.URL);
	    }

	    public void closeBrowser() {
	        getInstance().getDriver().quit();
	    }

	    public void manageTimeOut(int timeout) {
	    	driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
	    }

	   public ScreenShot getScreenShotCreator() {
	        return this.screenShotCreator;
	    }

	    public String getBrowser() {
	        return this.browser;
	    }

}
