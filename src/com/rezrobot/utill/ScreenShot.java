/**
 * 
 */
package com.rezrobot.utill;

/**
 * @author Dulan
 *
 */
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class ScreenShot {
    private static int imageIndex = 0;

    public ScreenShot() {
    }

    public void getScreenShotOnFailure() {
        String baseDirectory = this.getClass().getClassLoader().getResource("").getPath().split("target/")[0];
        String imageFolderPath = baseDirectory + "\\test-output\\images\\";
        File scrFile = (File)((TakesScreenshot)DriverFactory.getInstance().getDriver()).getScreenshotAs(OutputType.FILE);

        try {
            ++imageIndex;
            FileUtils.copyFile(scrFile, new File(imageFolderPath + imageIndex + ".png"));
        } catch (IOException var5) {
            var5.printStackTrace();
        }

    }
    
    public String getScreenShotPathOnFailure(String testDescription) {
    	
    	String path = "";
        //String baseDirectory = this.getClass().getClassLoader().getResource("").getPath().split("target/")[0];
        String baseDirectory = "target";
        String imageFolderPath = baseDirectory + "/test-output/images/";
        File scrFile = (File)((TakesScreenshot)DriverFactory.getInstance().getDriver()).getScreenshotAs(OutputType.FILE);

        try {
            ++imageIndex;
            path = imageFolderPath + imageIndex + testDescription + ".jpg";
            FileUtils.copyFile(scrFile, new File(path));
        } catch (IOException var5) {
            var5.printStackTrace();
        }

        return path;
    }
}