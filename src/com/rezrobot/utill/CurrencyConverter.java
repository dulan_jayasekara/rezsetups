package com.rezrobot.utill;

import java.math.BigDecimal;
import java.util.Map;

public class CurrencyConverter {

	public String convert(String portalCurrency, String value, String toCurrency, Map<String, String> currency, String inCurrency)
	{
		String 		returnValue 	= 	"0";
		if(currency.get(inCurrency).equals("null"))
		{
			returnValue = "error";
		}
		else
		{
			if(portalCurrency.equals(toCurrency) && portalCurrency.equals(inCurrency))
			{
				float	fvalue				= Float.parseFloat(value);
				BigDecimal roundfinalPrice 	= new BigDecimal(fvalue).setScale(0,BigDecimal.ROUND_CEILING);
						returnValue			= String.valueOf(roundfinalPrice);
			}
			else if(portalCurrency.equals(toCurrency) && !portalCurrency.equals(inCurrency))
			{
				float 	rate				= Float.parseFloat(currency.get(inCurrency)); 
				float 	fValue				= Float.parseFloat(value);	//fvalue	= converted value to float
				float 	cValue				= fValue/rate;				//cvalue	= conveted final value
				BigDecimal roundfinalPrice 	= new BigDecimal(cValue).setScale(0,BigDecimal.ROUND_CEILING);
						returnValue			= String.valueOf(roundfinalPrice);			
			}
			else if(portalCurrency.equals(inCurrency) && !portalCurrency.equals(toCurrency))
			{
				float 	rate				= Float.parseFloat(currency.get(toCurrency));
				float 	fValue				= Float.parseFloat(value);	//fvalue	= converted value to float
				float 	cValue				= fValue*rate;				//cvalue	= conveted final value
				BigDecimal roundfinalPrice 	= new BigDecimal(cValue).setScale(0,BigDecimal.ROUND_CEILING);
						returnValue			= String.valueOf(roundfinalPrice);			
			}
			else if(!portalCurrency.equals(inCurrency) && !portalCurrency.equals(toCurrency))
			{
				float 	crate				= Float.parseFloat(currency.get(toCurrency));
				float 	srate				= Float.parseFloat(currency.get(inCurrency));
				float 	fValue				= Float.parseFloat(value);	//fvalue	= converted value to float
				float 	cValue				= fValue/srate;
						cValue				= cValue*crate;
				BigDecimal roundfinalPrice 	= new BigDecimal(cValue).setScale(0,BigDecimal.ROUND_CEILING);
						returnValue			= String.valueOf(roundfinalPrice);
			}
		}
		
		// returns value in portal currency
		return returnValue;
	}
	
	public String convertWithoutRoundingUp(String portalCurrency, String value, String toCurrency, Map<String, String> currency, String inCurrency)
	{
		float 		returnValue 	= 	0;
		if(currency.get(inCurrency).equals("null"))
		{
			
		}
		else
		{
			if(portalCurrency.equals(toCurrency) && portalCurrency.equals(inCurrency))
			{
				float	fvalue				= Float.parseFloat(value);
						returnValue			= fvalue;
			}
			else if(portalCurrency.equals(toCurrency) && !portalCurrency.equals(inCurrency))
			{
				float 	rate				= Float.parseFloat(currency.get(inCurrency)); 
				float 	fValue				= Float.parseFloat(value);	//fvalue	= converted value to float
				float 	cValue				= fValue/rate;				//cvalue	= conveted final value
						returnValue			= cValue;			
			}
			else if(portalCurrency.equals(inCurrency) && !portalCurrency.equals(toCurrency))
			{
				float 	rate				= Float.parseFloat(currency.get(toCurrency));
				float 	fValue				= Float.parseFloat(value);	//fvalue	= converted value to float
				float 	cValue				= fValue*rate;				//cvalue	= conveted final value
						returnValue			= cValue;			
			}
			else if(!portalCurrency.equals(inCurrency) && !portalCurrency.equals(toCurrency))
			{
				float 	crate				= Float.parseFloat(currency.get(toCurrency));
				float 	srate				= Float.parseFloat(currency.get(inCurrency));
				float 	fValue				= Float.parseFloat(value);	//fvalue	= converted value to float
				float 	cValue				= fValue/srate;
						cValue				= cValue*crate;
						returnValue			= cValue;
			}
		}
		 BigDecimal bd = new BigDecimal(Float.toString(returnValue));
	     bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
		// returns value in portal currency
		return String.valueOf(bd.floatValue());
	}
	
	//CONVERT DOUBLE VALUE FROM ITS INCURRENCY TO ANY CURRENCY
	public static double convert(String InCurrency, String ToCurrency, String portalCurrency, double value, Map<String, String> currencyMap, boolean roundup)
	{
		String PortalCurrency = portalCurrency;
			
		double   returnValue  =  0;
		if(PortalCurrency.equals(InCurrency) && PortalCurrency.equals(ToCurrency))
		{
			double fvalue	= value;
			returnValue   	= fvalue;
		}
		if(PortalCurrency.equals(InCurrency) && !PortalCurrency.equals(ToCurrency))
		{
			double  rate    	= Double.parseDouble(currencyMap.get(ToCurrency)); 
			double  fvalue    	= value;//fvalue = converted value to float
			double  cValue    	= fvalue * rate;//cvalue = conveted final value
			returnValue   = cValue;   
		}
		if(PortalCurrency.equals(ToCurrency) && !PortalCurrency.equals(InCurrency))
		{
			//System.out.println(CurrencyMap.get(InCurrency));
			double  rate		= Double.parseDouble(currencyMap.get(InCurrency));
			double  fValue		= value; //fvalue = converted value to float
			double  cValue		= fValue / rate;    //cvalue = conveted final value
			returnValue   = cValue;   
		}
		if(!PortalCurrency.equals(ToCurrency) && !PortalCurrency.equals(InCurrency))
		{
			//System.out.println(CurrencyMap.get(InCurrency));
			double  crate		= Double.parseDouble(currencyMap.get(InCurrency));
			double  srate		= Double.parseDouble(currencyMap.get(ToCurrency));
			double  fValue		= value; //fvalue = converted value to float
			double  cValue		= fValue / srate; 
			cValue				= cValue*crate;
			returnValue		= cValue;
		}
	
		if(roundup)
		{
			returnValue = Math.ceil(returnValue);
		}
		return returnValue;
	}
		
	
}
