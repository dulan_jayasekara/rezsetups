package com.rezrobot.types;

public enum SummarizedBy {

	BOOKING, POS, CITY,COUNTRY,NONE;

	public static SummarizedBy getTourOperatorType(String reporttype) {

		if (reporttype.trim().equalsIgnoreCase("BOOKING"))
			return SummarizedBy.BOOKING;
		else if (reporttype.trim().equalsIgnoreCase("POS"))
			return SummarizedBy.POS;
		else if (reporttype.trim().equalsIgnoreCase("CITY"))
			return SummarizedBy.CITY;
		else if (reporttype.trim().equalsIgnoreCase("COUNTRY"))
			return SummarizedBy.COUNTRY;
		else
			return SummarizedBy.NONE;
	}
}
