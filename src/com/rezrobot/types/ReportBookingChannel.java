package com.rezrobot.types;

public enum ReportBookingChannel {

	ALL, POS, WEB,NONE;

	public static ReportBookingChannel getTourOperatorType(String reporttype) {

		if (reporttype.trim().equalsIgnoreCase("ALL"))
			return ReportBookingChannel.ALL;
		else if (reporttype.trim().equalsIgnoreCase("POS"))
			return ReportBookingChannel.POS;
		else if (reporttype.trim().equalsIgnoreCase("WEB"))
			return ReportBookingChannel.WEB;
		else
			return ReportBookingChannel.NONE;
	}
}
