package com.rezrobot.types;

public enum UserType {
	
	INTERNAL,ADMIN,POSUSER,B2B,HOTELPARTNER,ALL,NONE;
	
	public static UserType getUserType(String UserType)
	{
		if(UserType.equalsIgnoreCase("INTERNAL"))return INTERNAL;
		else if(UserType.equalsIgnoreCase("ADMIN"))return ADMIN;
		else if(UserType.equalsIgnoreCase("POSUSER"))return POSUSER;
		else if(UserType.equalsIgnoreCase("B2B"))return B2B;
		else if(UserType.equalsIgnoreCase("HOTELPARTNER"))return HOTELPARTNER;
		else if(UserType.equalsIgnoreCase("All"))return ALL;
		else return NONE;
		
	}

	
}
