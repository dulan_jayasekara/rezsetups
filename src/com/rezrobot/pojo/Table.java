package com.rezrobot.pojo;

import java.util.ArrayList;
import java.util.Iterator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.rezrobot.core.UIElement;

public class Table extends UIElement {
	
	
public int getNumberOfRaws() throws Exception{
return (getRawList().size() -1) ;	
}

public int getNumberOfColumns() throws Exception{
	return (getRawList().get(1).findElements(By.tagName("td")).size()) ;
}


public ArrayList<String> getTableData() throws Exception{

ArrayList<String> tableData = new ArrayList<>();	
ArrayList<WebElement>	rawList = getRawList();

for (int i = 1; i < rawList.size(); i++) {
	WebElement raw  = rawList.get(i);
	ArrayList<WebElement> colomnList = (ArrayList<WebElement>) raw.findElements(By.tagName("td"));
	
	String Data = "";
	
	for (Iterator iterator = colomnList.iterator(); iterator.hasNext();) {
		WebElement webElement = (WebElement) iterator.next();
		Data = Data.concat(webElement.getText().trim());
		
		if(iterator.hasNext()) 
			Data = Data.concat(",");
		
	}
	
	tableData.add(Data);
	}
   return tableData;	
}

public ArrayList<WebElement> getRawList() throws Exception{
	return (ArrayList<WebElement>) this.getWebElement().findElements(By.tagName("tr"));
}


}
