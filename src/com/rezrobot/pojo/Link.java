package com.rezrobot.pojo;

import com.rezrobot.core.UIElement;

public class Link extends UIElement {
    
    
    public String getUrl(){
	try {
	   return getAttribute("href");
	} catch (Exception e) {
	   return ("N/A");
	}
    }

}
