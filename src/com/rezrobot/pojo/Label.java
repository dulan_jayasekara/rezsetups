package com.rezrobot.pojo;

import com.rezrobot.core.UIElement;

public class Label extends UIElement {

	public String getLabelName(String key) {
		try {
			return this.getText();
		} catch (Exception e) {
			return null;
		}

	}
	
	public boolean getLabelMandatoryStatus(){
	 	try {
	    	  return this.getAttribute("data-required").equalsIgnoreCase("required") ? true : false;	    	    
	    } catch (Exception e) {
	    	    return false;
	    }
	}
}