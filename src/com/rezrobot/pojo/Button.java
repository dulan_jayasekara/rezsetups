package com.rezrobot.pojo;

import com.rezrobot.core.UIElement;
import com.rezrobot.utill.DriverFactory;

public class Button extends UIElement {
    
    public String  getButtonColor() throws Exception{
	    try {
	        return  this.getWebElement().getCssValue("color");
	    } catch (Exception var2) {
	        DriverFactory.getInstance().getScreenShotCreator().getScreenShotOnFailure();
	       // Utilities.pause(15000);
	        throw var2;
	    }
	}

}
