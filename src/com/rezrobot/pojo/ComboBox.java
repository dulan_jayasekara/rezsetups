package com.rezrobot.pojo;

import java.util.ArrayList;
import java.util.Iterator;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.rezrobot.core.UIElement;
import com.rezrobot.utill.DriverFactory;

public class ComboBox extends UIElement {
    
    public void  selectOptionByText(String Text) throws Exception{
	    try {
		Select select = new Select(this.getWebElement());
	        select.selectByVisibleText(Text);
	    } catch (Exception var2) {
	        DriverFactory.getInstance().getScreenShotCreator().getScreenShotOnFailure();
	       // Utilities.pause(15000);
	        throw var2;
	    }
	}
    
    
    public void  selectOptionByValue(String value) throws Exception{
	    try {
		Select select = new Select(this.getWebElement());
	        select.selectByValue(value);
	    } catch (Exception var2) {
	        DriverFactory.getInstance().getScreenShotCreator().getScreenShotOnFailure();
	       // Utilities.pause(15000);
	        throw var2;
	    }
	}
    
    public void  selectOptionByIndex(int index) throws Exception{
	    try {
		Select select = new Select(this.getWebElement());
	        select.selectByIndex(index);
	    } catch (Exception var2) {
	        DriverFactory.getInstance().getScreenShotCreator().getScreenShotOnFailure();
	       // Utilities.pause(15000);
	        throw var2;
	    }
	}
    
    public String  getDefaultSelectedOption() throws Exception{
	    try {
		Select select = new Select(this.getWebElement());
		String Option = select.getFirstSelectedOption().getText();
	       return  Option ;
	    } catch (Exception var2) {
	        DriverFactory.getInstance().getScreenShotCreator().getScreenShotOnFailure();
	       // Utilities.pause(15000);
	        throw var2;
	    }
	}
    
    
    public ArrayList<String>  getAvailableOptions() throws Exception{
    	
	    try {
		ArrayList<String> options = new ArrayList<>();
		Select select = new Select(this.getWebElement());
	        ArrayList<WebElement> optionlist = new ArrayList<>(select.getOptions());
	        for (Iterator<WebElement> iterator = optionlist.iterator(); iterator
			.hasNext();) {
	           
		    WebElement webElement = (WebElement) iterator.next();
		    options.add(webElement.getText());
		    
		}
	        
	        return options;
	    } catch (Exception var2) {
	        DriverFactory.getInstance().getScreenShotCreator().getScreenShotOnFailure();
	       // Utilities.pause(15000);
	        throw var2;
	    }
	}
    
    public boolean getComboBoxMandatoryStatus(){
     	try {
        	  return this.getAttribute("required").equalsIgnoreCase("") ? false : true;
        	    
        	} catch (Exception e) {
        	    return false;
        	}
     }
    
    public boolean getComboBoxMandatoryStatusFromDataConstraints(){
     	try {
        	  return this.getAttribute("data-constraints").contains("required") ? true : false;
        	    
        	} catch (Exception e) {
        	    return false;
        	}
    }

}
