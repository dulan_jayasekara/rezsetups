package com.rezrobot.pojo;

import com.rezrobot.core.UIElement;

public class CheckBox extends UIElement {
	
	public boolean isElementSelected(){
    	
    	try {
			return this.getWebElement().isSelected();
		} catch (Exception e) {
			return false;
		}   	
    }
	
	public boolean getCheckBoxMandatoryStatusFromDataConstraints(){
     	try {
        	  return this.getAttribute("data-constraints").contains("required") ? true : false;
        	    
        	} catch (Exception e) {
        	    return false;
        	}
    }
	

}
