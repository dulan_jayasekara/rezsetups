package com.rezrobot.pojo;

import com.rezrobot.core.UIElement;
import com.rezrobot.utill.DriverFactory;

public class RadioButton extends UIElement {
	
    
    public boolean getRadioButtonMandatoryStatus(){
     	
    	try {
        	  return this.getAttribute("required").equalsIgnoreCase("") ? false : true;
    	} catch (Exception e) {
    	    return false;
    	}	    
        	
     }
    
    public boolean isElementSelected(){
    	
    	try {
			return this.getWebElement().isSelected();
		} catch (Exception e) {
			return false;
		}   	
    }
    
    public boolean getRadioButtonMandatoryStatusFromDataConstraints(){
     	
    	try {
        	  return this.getAttribute("data-constraints").contains("required") ? false : true;
    	} catch (Exception e) {
    	    return false;
    	}	    
        	
     }

}
