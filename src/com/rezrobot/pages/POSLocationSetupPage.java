package com.rezrobot.pages;

import java.util.HashMap;

import com.rezrobot.core.PageObject;
import com.rezrobot.core.UIElement;
import com.rezrobot.interfaces.Forms;
import com.rezrobot.interfaces.Reports;
import com.rezrobot.pojo.ComboBox;
import com.rezrobot.pojo.Image;

public class POSLocationSetupPage extends PageObject implements Forms, Reports {
	
	public boolean isPageAvailable() {
		return super.isPageAvailable("inputfield_POSLocationCode_id");
	}

	public boolean getLogoAvailability() {
		try {
			return ((Image) getElement("")).isImageLoaded();
		} catch (Exception e) {
			return false;
		}
	}
	
	public UIElement getElement(String key){
	 	try {
	 		return this.objectMap.get(key);
	    } catch (Exception e) {
	    	throw e;
	    }
	}
	
	public HashMap<String, String> getLabels() {

		HashMap<String, String> labels = new HashMap<>();
		
		labels.put("Where", getLabelName("label_whereGoingLabel_xpath"));


		return labels;
	}
	
	public HashMap<String, String> getPlaceHolders() {

		HashMap<String, String> placeholders = new HashMap<>();
		
	    try {
	    	
	    	placeholders.put("Country of residance", ((ComboBox)getElement("combobox_ResidenceCountry_id")).getDefaultSelectedOption().trim());

		} catch (Exception e) {
			// TODO: handle exception
		}

		return placeholders;
	}
	
	public HashMap<String, Boolean> getAvailabilityOfInputFields() {

		HashMap<String, Boolean> fileds = new HashMap<>();
		try {
			fileds.put("POSLocationCode", getElementVisibility("inputfield_POSLocationCode_id"));
			fileds.put("POSLocationName", getElementVisibility("inputfield_POSLocationName_id"));
			fileds.put("CurrencyCode", getElementVisibility("inputfield_CurrencyCode_id"));
			fileds.put("Address1", getElementVisibility("inputfield_Address1_id"));
			fileds.put("Address2", getElementVisibility("inputfield_Address2_id"));
			fileds.put("Country", getElementVisibility("inputfield_Country_id"));
			fileds.put("State", getElementVisibility("inputfield_State_id"));
			fileds.put("City", getElementVisibility("inputfield_City_id"));
			fileds.put("ZipCode", getElementVisibility("inputfield_ZipCode_id"));
			fileds.put("POSGroupCode", getElementVisibility("inputfield_POSGroupCode_id"));
			
		} catch (Exception e) {
			// TODO: handle exception
		}

		return fileds;
	}
	
	public HashMap<String, Boolean> getMandatoryList() {

		HashMap<String, Boolean> fileds = new HashMap<>();

		try {
			fileds.put("POSLocationCode", getMandatoryStatus("inputfield_POSLocationCode_id"));
			fileds.put("POSLocationName", getMandatoryStatus("inputfield_POSLocationName_id"));
			fileds.put("CurrencyCode", getMandatoryStatus("inputfield_CurrencyCode_id"));
			fileds.put("CurrencyCodeLookup", getMandatoryStatus("image_CurrencyCode_id"));
			fileds.put("Address1", getMandatoryStatus("inputfield_Address1_id"));
			fileds.put("Country", getMandatoryStatus("inputfield_Country_id"));
			fileds.put("CountryLookup", getMandatoryStatus("image_Country_id"));
			fileds.put("City", getMandatoryStatus("inputfield_City_id"));
			fileds.put("CityLookup", getMandatoryStatus("image_City_id"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return fileds;
	}
	
	public HashMap<String, Boolean> getAvailabilityOfRadioButtons() {
		
		HashMap<String, Boolean> radioButtons = new HashMap<>();
		
		try {
			radioButtons.put("CreatePOSLocation", getElementVisibility("radiobutton_CreatePOSLocation_id"));
			radioButtons.put("ModifyPOSLocation", getElementVisibility("radiobutton_ModifyPOSLocation_id"));
			radioButtons.put("DeletePOSLocation", getElementVisibility("radiobutton_DeletePOSLocation_id"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return radioButtons;
	}
	
	public HashMap<String, Boolean> getLookupList(){
		
		HashMap<String, Boolean> lookupImages = new HashMap<>();
		
		
		
		return lookupImages;
	}
}
