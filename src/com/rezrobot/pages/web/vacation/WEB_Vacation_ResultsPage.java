package com.rezrobot.pages.web.vacation;

import java.util.ArrayList;
import java.util.HashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.rezrobot.core.PageObject;
import com.rezrobot.core.UIElement;
import com.rezrobot.dataobjects.HotelDetails;
import com.rezrobot.dataobjects.HotelResultsPageMoreDetails;
import com.rezrobot.dataobjects.HotelRoomDetails;
import com.rezrobot.dataobjects.HotelSearchObject;
import com.rezrobot.dataobjects.Vacation_Search_object;
import com.rezrobot.processor.LabelReadProperties;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.ReportTemplate;

public class WEB_Vacation_ResultsPage extends PageObject{

	public boolean isPageLoaded() throws Exception
	{
		switchToDefaultFrame();
		try {
			getElementVisibility("inputfield_departure_id", 30);
			return getelementAvailability("inputfield_departure_id");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			return false;
		}
		
	}
	
	public boolean isResultsAvailable() throws Exception
	{
		switchToDefaultFrame();
		getElementVisibility("label_outflightdepflightcode_id", 10);
		getElementVisibility("label_hotel_block_id", 10);
		getElementVisibility("label_hotel_unavailable_error_type_id", 20);
		if(getelementAvailability("label_hotel_unavailable_error_type_id"))
		{
			System.out.println(getElement("label_hotel_unavailable_error_id").getText());
			getElement("button_hotel_unavailable_error_close_xpath").click();
			return false;
		}
		else
		{
			if(getElementVisibility("label_outflightdepflightcode_id") && getElementVisibility("label_hotel_block_id"))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	public HashMap<String, String> getSelectedFlightDetails()
	{
		HashMap<String, String> values = new HashMap<>();
		try {
			values.put("outflightdepflightcode", 	getElement("label_outflightdepflightcode_id").getText());
			values.put("outflightdepdate", 			getElement("label_outflightdepdate_id").getText());
			values.put("outflightdeplocation", 		getElement("label_outflightdeplocation_id").getText().replace("(", "").replace(")", ""));
			values.put("outflightarrflightcode", 	getElement("label_outflightarrflightcode_id").getText());
			values.put("outflightarrtime", 			getElement("label_outflightarrtime_id").getText());
			values.put("outflightarrlocation", 		getElement("label_outflightarrlocation_id").getText().replace("(", "").replace(")", ""));
			values.put("outflightdeptime", 			getElement("label_outflightdeptime_id").getText());
			values.put("outflightarrdate", 			getElement("label_outflightarrdate_id").getText());
			values.put("outflightarrtime", 			getElement("label_outflightarrtime_id").getText());
			values.put("inbounddepfligthcode", 		getElement("label_inbounddepfligthcode_id").getText());
			values.put("inbounddeploaction", 		getElement("label_inbounddeploaction_id").getText().replace("(", "").replace(")", ""));
			values.put("inbounddepdate", 			getElement("label_inbounddepdate_id").getText());
			values.put("inbounddeptime", 			getElement("label_inbounddeptime_id").getText());
			values.put("inboundarrflightcode", 		getElement("label_inboundarrflightcode_id").getText());
			values.put("inboundarrlocation",	 	getElement("label_inboundarrlocation_id").getText().replace("(", "").replace(")", ""));
			values.put("inboundarrdate", 			getElement("label_inboundarrdate_id").getText());
			values.put("inboundarrtime", 			getElement("label_inboundarrtime_id").getText());
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("get selsected flight details error - " + e);
		}
		try {
			WebElement element = DriverFactory.getInstance().getDriver().findElement(By.id("flight-tracer-id"));
			String contents = (String)((JavascriptExecutor)DriverFactory.getInstance().getDriver()).executeScript("return arguments[0].innerHTML;", element); 
			System.out.println("flight tracer id - " + contents);
			values.put("flightTracer", contents);
			
	//		String abc = (String) ((JavascriptExecutor)DriverFactory.getInstance().getDriver()).executeScript("document.getElementById('flight-tracer-id').innerHTML;");
			//string descriptionTextXPath = "//div[contains(@class, 'description')]/h3";
			System.out.println(DriverFactory.getInstance().getDriver().findElement(By.id("vac_flight_WJ_1")).findElements(By.tagName("p")).size());
			System.out.println(DriverFactory.getInstance().getDriver().findElement(By.xpath("//div[contains(@id, 'vac_flight_WJ_1')]/span")).getText());
			System.out.println(DriverFactory.getInstance().getDriver().findElements(By.tagName("span")).size());
			for(int i = 0 ; i < DriverFactory.getInstance().getDriver().findElement(By.id("vac_flight_WJ_1")).findElements(By.tagName("span")).size() ; i++)
			{
				System.out.println(DriverFactory.getInstance().getDriver().findElement(By.id("vac_flight_WJ_1")).findElements(By.tagName("span")).get(i).getText());
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		System.out.println(values.get("flightTracer"));
		return values;
	}
	
	public ArrayList<HotelDetails> getAllHotelDetails() throws Exception
	{
		String 		hotelCount 			= getElement("label_number_of_hotels_xpath").getText();
		int 		pageCount 			= 0;
		
		
		if(Integer.parseInt(hotelCount)%10 == 0)
		{
			pageCount = Integer.parseInt(hotelCount)/10;
		}
		else
		{
			pageCount = Integer.parseInt(hotelCount)/10;
			pageCount = pageCount + 1;
		}
		System.out.println(pageCount);
		String 	moreDetails[];
		String 	hotelID = "";
		String 	supplier = "";
		int 	count = 1;
		UIElement changeRef = new UIElement();
		ArrayList<HotelDetails> hotelList = new ArrayList<HotelDetails>();
		pageIterator : for(int pageIterator = 1 ; pageIterator <= pageCount ; pageIterator++)
		{
			try {
				executejavascript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+pageIterator+"},'','WJ_3'))");
			} catch (Exception e) {
				// TODO: handle exception
				break;
			}
			// get all hotel details
			resultsIterator : for(int resultsIterator = 1 ; resultsIterator <= 10 ; resultsIterator++)
			{
				HotelDetails hotel = new HotelDetails();
				try {
					changeRef = getElement("button_more_details_xpath").changeRefAndGetElement(getElement("button_more_details_xpath").getRef().replace("_1", "_".concat(String.valueOf(count))));
					moreDetails = changeRef.getAttribute("href").replace("'", "").split(",");
					hotelID = moreDetails[1];
					supplier = moreDetails[3].replace(")", "");
					hotel.setHotelID(hotelID);
					changeRef = getElement("label_hotel_name_id").changeRefAndGetElement(getElement("label_hotel_name_id").getRef().replace("1", String.valueOf(count)));
					hotel.setTitle(changeRef.getText());
					changeRef = getElement("label_hotel_address_xpath").changeRefAndGetElement(getElement("label_hotel_address_xpath").getRef().replace("_1", "_".concat(String.valueOf(count))));
					hotel.setAddress(changeRef.getText());
					changeRef = getElement("label_hotel_rate_id").changeRefAndGetElement(getElement("label_hotel_rate_id").getRef().concat(hotelID).concat("-".concat(String.valueOf(count))));
					hotel.setRate(changeRef.getText());
					hotel.setSupplier(supplier);
					hotelList.add(hotel);
					
				} catch (Exception e) {
					// TODO: handle exception
					break;
				}
				count++;
			}
		}
		
		return hotelList;
	}
	
	public HotelDetails getSelectedHotelInfo(Vacation_Search_object search, ArrayList<HotelDetails> hotelList, LabelReadProperties labelProperty, ReportTemplate printTemplate) throws Exception
	{
		HotelDetails hotel = new HotelDetails();
		String tracerID = "";
		String starRating = "";
		int timeout = 10;
		int internal = 0;
		int hb = 0;
		int gta = 0;
		int tourico = 0;
		int rezlive = 0;
		int hotelspro = 0;
		
		String 		hotelCount 			= getElement("label_number_of_hotels_xpath").getText();
		int 		pageCount 			= 0;
		
		
		if(Integer.parseInt(hotelCount)%10 == 0)
		{
			pageCount = Integer.parseInt(hotelCount)/10;
		}
		else
		{
			pageCount = Integer.parseInt(hotelCount)/10;
			pageCount = pageCount + 1;
		}
		try {
			ArrayList<HotelRoomDetails> roomList = new ArrayList<HotelRoomDetails>();
			
			for(int i = 0 ; i < hotelList.size() ; i++)
			{
				if(hotelList.get(i).getSupplier().equals("hotelbeds_v2"))
					hb++;
				else if(hotelList.get(i).getSupplier().equals("INV27"))
					gta++;
				else if(hotelList.get(i).getSupplier().equals("INV13"))
					tourico++;
				else if(hotelList.get(i).getSupplier().equals("rezlive"))
					rezlive++;
				else if(hotelList.get(i).getSupplier().equals("hotelspro"))
					hotelspro++;
				else
					internal++;
			}
			System.out.println("HB - " + hb);
			System.out.println("inv27 - " + gta);
			System.out.println("inv13 - " + tourico);
			System.out.println("rezlive - " + rezlive);
			System.out.println("Hotelspro - " + hotelspro);
			System.out.println("internal - " + internal);
//			printTemplate.setInfoTableHeading("Add to cart issues");
			String hotelName = "";
			String hotelID = "";
			for(int i = 0 ; i < hotelList.size() ; i++)
			{
				if(search.getHotelSupplier().equals(hotelList.get(i).getSupplier()))
				{
					hotelName 	= hotelList.get(i).getTitle();
					hotelID 	= hotelList.get(i).getHotelID();
					break;
				}
			}
			
			int 	count = 1;
			pageIterator : for(int pageIterator = 1 ; pageIterator <= pageCount ; pageIterator++)
			{
				try {
					executejavascript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+pageIterator+"},'','WJ_3'))");
				} catch (Exception e) {
					// TODO: handle exception
					break;
				}
				// get all hotel details
				resultsIterator : for(int resultsIterator = 1 ; resultsIterator <= 10 ; resultsIterator++)
				{
					if(getElement("label_hotel_name_id").changeRefAndGetElement(getElement("label_hotel_name_id").getRef().replace("1", String.valueOf(count))).getText().equals(hotelName))
					{

						hotel.setTitle(hotelName);
						String moreDetails[] = getElement("button_more_details_xpath").changeRefAndGetElement(getElement("button_more_details_xpath").getRef().replace("_1", "_" + String.valueOf(count))).getAttribute("href").split("'");
						hotel.setHotelID(moreDetails[3]);
						System.out.println(moreDetails[1]);
						hotel.setDestination(moreDetails[1]);
						hotel.setSupplier(moreDetails[7]);
						
						
						WebElement element = DriverFactory.getInstance().getDriver().findElement(By.id("hotel_"+ String.valueOf(count)));
						//get star rating
						for(int i = 0 ; i <= 7 ; i++)
						{
							if(element.findElements(By.className("hotel_star" + String.valueOf(i))).size() > 0)
							{
								starRating = String.valueOf(i);
								break;
							}	
						}
						System.out.println(starRating);
						hotel.setStarRating(starRating);
						String rate[] = getElement("label_hotel_rate_id").changeRefAndGetElement(getElement("label_hotel_rate_id").getRef().concat(hotel.getHotelID()) + "-" + String.valueOf(count)).getText().split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
						hotel.setRate(rate[1]);
						hotel.setCurrency(rate[0]);
						hotel.setAddress(getElement("label_hotel_address_xpath").changeRefAndGetElement(getElement("label_hotel_address_xpath").getRef().replace("_1", "_" + String.valueOf(count))).getText());
						try {
							getElement("button_show_more_rooms_id").changeRefAndGetElement(getElement("button_show_more_rooms_id").getRef().replace("replace", String.valueOf(count))).click();
						} catch (Exception e) {
							// TODO: handle exception
						}
						
						int roomCount = 0;
						for(int i = 1 ; i < 100 ; i++)
						{
							try {
								if(!getElement("label_rate_plan_id").changeRefAndGetElement(getElement("label_rate_plan_id").getRef().replace("replace1", String.valueOf(count)).replace("replace2", String.valueOf(i))).getText().equals(""))
								{
									roomCount++;
								}
							} catch (Exception e) {
								// TODO: handle exception
								break;
							}
						}
						//get room details
						try {
							for(int i = 1 ; i  <= roomCount ; i++)
							{
								HotelRoomDetails room = new HotelRoomDetails();
								room.setRoomType(getElement("label_hotel_room_type_id").changeRefAndGetElement(getElement("label_hotel_room_type_id").getRef().replace("replace1", String.valueOf(count)).replace("replace2", String.valueOf(i))).getText());
								room.setRatePlan(getElement("label_rate_plan_id").changeRefAndGetElement(getElement("label_rate_plan_id").getRef().replace("replace1", String.valueOf(count)).replace("replace2", String.valueOf(i))).getText());
								roomList.add(room);
							}
							hotel.setRoomList(roomList);
						} catch (Exception e) {
							// TODO: handle exception
							System.out.println("Get room details error - " + e);
						}
						
						
						// get more details
						System.out.println(getElement("button_more_details_xpath").changeRefAndGetElement(getElement("button_more_details_xpath").getRef().replace("_1", "_" + String.valueOf(count))).getRef());
						getElement("button_more_details_xpath").changeRefAndGetElement(getElement("button_more_details_xpath").getRef().replace("_1", "_" + String.valueOf(count))).click();
						 
						// check more details availability
						HotelResultsPageMoreDetails moreinfo = new HotelResultsPageMoreDetails();
						try {
							if(getElement("button_overview_xpath").changeRefAndGetElement(getElement("button_overview_xpath").getRef().replace("replace", hotelID)).isElementVisible(timeout))
							{
								moreinfo.setMoreInfoAvailability(true);
								// overview
								try {
									moreinfo.setOverview(getElement("label_overview_xpath").changeRefAndGetElement(getElement("label_overview_xpath").getRef().replace("replace", hotelID)).getText());
									moreinfo.setImageAvailability(getElement("image_more_info_hotel_image_xpath").changeRefAndGetElement(getElement("image_more_info_hotel_image_xpath").getRef().replace("replace", hotelID)).isElementVisible(timeout));
								} catch (Exception e) {
									// TODO: handle exception
								}
								
								
								// location on map
								try {
									getElement("button_location_on_map_xpath").changeRefAndGetElement(getElement("button_location_on_map_xpath").getRef().replace("replace", hotelID)).click();
									moreinfo.setMapAvailability(getElement("image_more_info_map_xpath").changeRefAndGetElement(getElement("image_more_info_map_xpath").getRef().replace("replace", hotelID)).isElementVisible(timeout));
								} catch (Exception e) {
									// TODO: handle exception
								}
								
							
								// amenities
								try {
									getElement("button_amenities_xpath").changeRefAndGetElement(getElement("button_amenities_xpath").getRef().replace("replace", hotelID)).click();
									ArrayList<String> amenityList = new ArrayList<String>();
									if(getElement("label_amenities_xpath").changeRefAndGetElement(getElement("label_amenities_xpath").getRef().replace("replace1", hotelID).replace("replace2", "1")).isElementVisible())
									{
										moreinfo.setAmanityAvailability(true);
										for(int i = 1 ; i <= getElement("label_more_info_amenities_classname").getWebElements().size() ; i++)
										{
											amenityList.add(getElement("label_amenities_xpath").changeRefAndGetElement(getElement("label_amenities_xpath").getRef().replace("replace1", hotelID).replace("replace2", String.valueOf(i))).getText());
										}
										moreinfo.setAmenityList(amenityList);
									}
									else
									{
										moreinfo.setAmanityAvailability(false);
									}
								} catch (Exception e) {
									// TODO: handle exception
								}
								
								//additional info
								try {
									getElement("button_additional_info_xpath").changeRefAndGetElement(getElement("button_additional_info_xpath").getRef().replace("replace", hotelID)).click();
									moreinfo.setAdditionalInfo(getElement("label_more_info_additional_info_xpath").changeRefAndGetElement(getElement("label_more_info_additional_info_xpath").getRef().replace("replace", hotelID)).getText());
								} catch (Exception e) {
									// TODO: handle exception
								}
								//send email
								try {
									getElement("button_email_to_friend_xpath").changeRefAndGetElement(getElement("button_email_to_friend_xpath").getRef().replace("replace", hotelID)).click();
									getElement("inputfield_email_address_id").setText("akila@colombo.rezgateway.com");//set dynamically
									hotel.setEmailSubject(getElement("inputfield_email_subject_id").getAttribute("value"));
									hotel.setEmailBody(getElement("inputfield_email_body_id").getText());
									getElement("button_send_email_xpath").click();
									Thread.sleep(3000);
									hotel.setEmailStatus(getElement("label_email_is_sent_id").getText()); 
									Thread.sleep(2000);
									getElement("button_email_sent_confirmation_xpath").click();
								} catch (Exception e) {
									// TODO: handle exception
									System.out.println("Mail sending error - " + e);
								} 
								
							}
							else
							{
								moreinfo.setMoreInfoAvailability(false);
							}
						} catch (Exception e) {
							// TODO: handle exception
						}
						hotel.setMoreInfo(moreinfo);
						
						
						hotel.setTracerID(getElement("tracerID").changeRefAndGetElement(getElement("tracerID").getRef().concat(hotelID)).getAttribute("value"));
						System.out.println("Tracer ID - " + hotel.getTracerID());
						try {
							System.out.println("Results page - Click Book button");
							Thread.sleep(2000);
							getElement("button_book_id").changeRefAndGetElement(getElement("button_book_id").getRef().replace("replace", String.valueOf(count))).click();
							Thread.sleep(2000);
							System.out.println("Results page - Waiting for rate change");
							if(getElementVisibility("label_cannot_add_this_combination_id", 30))
							{
//								printTemplate.addToInfoTabel(hotelName.get(j), getElement("label_cannot_add_this_combination_id").getText());
								getElement("button_cannot_add_this_combination_xpath").click();
								try {
									getElement("button_cannot_add_this_combination_xpath").click();
								} catch (Exception e) {
									// TODO: handle exception
								}
							}
							else if(getElementVisibility("label_rate_got_changed_id", 10))
							{
//								printTemplate.addToInfoTabel(hotelName.get(j), getElement("label_rate_got_changed_id").getText());
								hotel.setIsRateChangeAvailable(true);
								getElement("button_accept_rate_change_xpath").click();
							}
							else
							{
//								printTemplate.addToInfoTabel(hotelName.get(j), "Add to cart was successful");
								break;
							}
						} catch (Exception e) {
							// TODO: handle exception
							System.out.println("add to cart error - " + e);
						}
					
					}
					count++;
				}
			}
//			printTemplate.markTableEnd();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		
		
		return hotel;
		
	}
	
	public boolean isSelectedHotelResultsAvailable() {
		try {
			switchToDefaultFrame();
			return getElementVisibility("label_hotel_name_id", 100);		
		} catch (Exception e) {
			switchToDefaultFrame();
			return false;
		}		
	}
	
}
