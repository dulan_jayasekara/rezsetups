package com.rezrobot.core;

/**
 * @author Dulan
 *
 */

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.rezrobot.pojo.ComboBox;
import com.rezrobot.utill.DriverFactory;

public class UIElement {
	private Logger logger;
	private String Key;
	private String refType;
	private String Ref;
	private String Remarks;

	
	public void setRemarks(String remarks) {
		Remarks= remarks;
	}
	public String getRemarks() {
		return Remarks;
	}
	public void setRef(String ref) {
		Ref = ref;
	}

	public UIElement() {
		logger = Logger.getLogger(this.getClass());
	}

	public void setElementProperties(String Key, String RefType, String Ref) {
		this.Key = Key;
		this.refType = RefType;
		this.Ref = Ref;
		
		logger.debug("Setting element properties -> Key : " + Key
				+ " RefType : " + RefType + " Ref : " + Ref);
	}

	public String getKey() {
		return Key;
	}

	public String getRefType() {
		return refType;
	}

	public String getRef() {
		return Ref;
	}

	public WebElement getWebElement() throws Exception {

		By by;

		if (RefType.getType(this.refType) == RefType.ID)
			by = By.id(this.Ref);
		else if (RefType.getType(this.refType) == RefType.XPATH)
			by = By.xpath(this.Ref);
		else if (RefType.getType(this.refType) == RefType.CLASSNAME)
			by = By.className(this.Ref);
		else if (RefType.getType(this.refType) == RefType.CSS)
			by = By.cssSelector(this.Ref);
		else if (RefType.getType(this.refType) == RefType.LINKTEXT)
			by = By.linkText(this.Ref);
		else if (RefType.getType(this.refType) == RefType.PARTIALLINKTEXT)
			by = By.partialLinkText(this.Ref);
		else if (RefType.getType(this.refType) == RefType.NAME)
			by = By.name(this.Ref);
		else if (RefType.getType(this.refType) == RefType.TAGNAME)
			by = By.tagName(this.Ref);
		else {
			throw new Exception(
					"Ref type not defined in the xml or invalid ref type :"
							+ refType);
		}

		return DriverFactory.getInstance().getDriver().findElement(by);

	}
	
	public WebElement getinnerWebElement(WebElement webelem,UIElement innerelement) throws Exception {

		By by;

		if (RefType.getType(innerelement.refType) == RefType.ID)
			by = By.id(innerelement.Ref);
		else if (RefType.getType(innerelement.refType) == RefType.XPATH)
			by = By.xpath(innerelement.Ref);
		else if (RefType.getType(innerelement.refType) == RefType.CLASSNAME)
			by = By.className(innerelement.Ref);
		else if (RefType.getType(innerelement.refType) == RefType.CSS)
			by = By.cssSelector(innerelement.Ref);
		else if (RefType.getType(innerelement.refType) == RefType.LINKTEXT)
			by = By.linkText(innerelement.Ref);
		else if (RefType.getType(innerelement.refType) == RefType.PARTIALLINKTEXT)
			by = By.partialLinkText(innerelement.Ref);
		else if (RefType.getType(innerelement.refType) == RefType.NAME)
			by = By.name(innerelement.Ref);
		else if (RefType.getType(innerelement.refType) == RefType.TAGNAME)
			by = By.tagName(innerelement.Ref);
		else {
			throw new Exception(
					"Ref type not defined in the xml or invalid ref type :"
							+ refType);
		}

		return webelem.findElement(by);

	}
	
	
	public List<WebElement> getWebElements() throws Exception {

		By by;
		if (RefType.getType(this.refType) == RefType.ID)
			by = By.id(this.Ref);
		else if (RefType.getType(this.refType) == RefType.XPATH)
			by = By.xpath(this.Ref);
		else if (RefType.getType(this.refType) == RefType.CLASSNAME)
			by = By.className(this.Ref);
		else if (RefType.getType(this.refType) == RefType.CSS)
			by = By.cssSelector(this.Ref);
		else if (RefType.getType(this.refType) == RefType.LINKTEXT)
			by = By.linkText(this.Ref);
		else if (RefType.getType(this.refType) == RefType.PARTIALLINKTEXT)
			by = By.partialLinkText(this.Ref);
		else if (RefType.getType(this.refType) == RefType.NAME)
			by = By.name(this.Ref);
		else if (RefType.getType(this.refType) == RefType.TAGNAME)
			by = By.tagName(this.Ref);
		else {
			throw new Exception(
					"Ref type not defined in the xml or invalid ref type :"
							+ refType);
		}

		return DriverFactory.getInstance().getDriver().findElements(by);

	}
	
	public List<WebElement> getInnerWebElements(WebElement webelem,UIElement innerelement) throws Exception {

		By by;

		if (RefType.getType(innerelement.refType) == RefType.ID)
			by = By.id(innerelement.Ref);
		else if (RefType.getType(innerelement.refType) == RefType.XPATH)
			by = By.xpath(innerelement.Ref);
		else if (RefType.getType(innerelement.refType) == RefType.CLASSNAME)
			by = By.className(innerelement.Ref);
		else if (RefType.getType(innerelement.refType) == RefType.CSS)
			by = By.cssSelector(innerelement.Ref);
		else if (RefType.getType(innerelement.refType) == RefType.LINKTEXT)
			by = By.linkText(innerelement.Ref);
		else if (RefType.getType(innerelement.refType) == RefType.PARTIALLINKTEXT)
			by = By.partialLinkText(innerelement.Ref);
		else if (RefType.getType(innerelement.refType) == RefType.NAME)
			by = By.name(innerelement.Ref);
		else if (RefType.getType(innerelement.refType) == RefType.TAGNAME)
			by = By.tagName(innerelement.Ref);
		else {
			throw new Exception(
					"Ref type not defined in the xml or invalid ref type :"
							+ refType);
		}

		return webelem.findElements(by);

	}
	
	public By getByType() throws Exception {

		By by;

		if (RefType.getType(this.refType) == RefType.ID)
			by = By.id(this.Ref);
		else if (RefType.getType(this.refType) == RefType.XPATH)
			by = By.xpath(this.Ref);
		else if (RefType.getType(this.refType) == RefType.CLASSNAME)
			by = By.className(this.Ref);
		else if (RefType.getType(this.refType) == RefType.CSS)
			by = By.cssSelector(this.Ref);
		else if (RefType.getType(this.refType) == RefType.LINKTEXT)
			by = By.linkText(this.Ref);
		else if (RefType.getType(this.refType) == RefType.PARTIALLINKTEXT)
			by = By.partialLinkText(this.Ref);
		else if (RefType.getType(this.refType) == RefType.NAME)
			by = By.name(this.Ref);
		else if (RefType.getType(this.refType) == RefType.TAGNAME)
			by = By.tagName(this.Ref);
		else {
			throw new Exception(
					"Ref type not defined in the xml or invalid ref type :"
							+ refType);
		}

		return by;

	}

	public UIElement changeRefAndGetElement(String ref)
	{
		/*UIElement element ;
		if(ComboBox.class instanceof this.getClass())
		{
			
		}
		
		if(this.getClass().isInstance(ComboBox.class)){
		      element = new ComboBox();
		}else if(this.getClass().isInstance(InputFiled.class)){
			  element = new InputFiled();
		}else if(this.getClass().isInstance(Label.class)){
				element = new  Label();
		}else{
			element = new UIElement();
		}*/
		ComboBox element = new ComboBox();	
		
		element.setRef(ref);
		element.setKey(this.Key);
		element.setRefType(this.refType);
		return element;
	}
	
	public void click() throws Exception {
		try {
			this.getWebElement().click();
		} catch (Exception var2) {
			DriverFactory.getInstance().getScreenShotCreator()
					.getScreenShotOnFailure();
			// Utilities.pause(15000);
			throw var2;
		}
	}

	public void setKey(String key) {
		Key = key;
	}

	public void setRefType(String refType) {
		this.refType = refType;
	}

	public void setText(String Text) throws Exception {
		try {
			this.getWebElement().sendKeys(new CharSequence[] { Text });
		} catch (Exception var2) {
			DriverFactory.getInstance().getScreenShotCreator()
					.getScreenShotOnFailure();
			// Utilities.pause(15000);
			throw var2;
		}
	}
	
	public void clear() throws Exception {
		try {
			this.getWebElement().clear();
		} catch (Exception var2) {
			DriverFactory.getInstance().getScreenShotCreator()
					.getScreenShotOnFailure();
			// Utilities.pause(15000);
			throw var2;
		}
	}
	
	public String getText() throws Exception {
		try {
			return this.getWebElement().getText();
		} catch (Exception var2) {
			DriverFactory.getInstance().getScreenShotCreator()
					.getScreenShotOnFailure();
			// Utilities.pause(15000);
			throw var2;
		}
	}
	
	public String getPlaceHolder() throws Exception {
		try {
			return this.getAttribute("placeholder");
		} catch (Exception var2) {
			DriverFactory.getInstance().getScreenShotCreator()
					.getScreenShotOnFailure();
			// Utilities.pause(15000);
			throw var2;
		}
	}

	public String getAttribute(String Attr) throws Exception {
		try {
			return this.getWebElement().getAttribute(Attr);
		} catch (Exception var2) {
			DriverFactory.getInstance().getScreenShotCreator()
					.getScreenShotOnFailure();
			// Utilities.pause(15000);
			throw var2;
		}
	}

	public String getColor() throws Exception {
		try {
			return this.getWebElement().getCssValue("color");
		} catch (Exception var2) {
			DriverFactory.getInstance().getScreenShotCreator()
					.getScreenShotOnFailure();
			// Utilities.pause(15000);
			throw var2;
		}
	}

	public void scrollUntilElementVisible() throws Exception {
		try {
			this.setJavaScript("arguments[0].scrollIntoView(true);",
					this.getWebElement());
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void setJavaScript(String script) throws Exception {
		try {
			if (DriverFactory.getInstance().getDriver() instanceof JavascriptExecutor) {
				((JavascriptExecutor) DriverFactory.getInstance().getDriver())
						.executeScript(script, new Object[0]);
			}
		} catch (Exception var3) {
			logger.fatal("Framework Error : Java Script Executed" + script);
			logger.fatal("Framework Error :Unable to Execute Java Script", var3);

		}
	}

	public void setJavaScript(String script, WebElement element)
			throws Exception {
		try {
			if (DriverFactory.getInstance().getDriver() instanceof JavascriptExecutor) {
				((JavascriptExecutor) DriverFactory.getInstance().getDriver())
						.executeScript(script, new Object[] { element });
			}
		} catch (Exception var3) {
			logger.fatal("Framework Error : Java Script Executed" + script);
			logger.fatal("Framework Error :Unable to Execute Java Script", var3);

		}
	}

	public Dimension getDimentions() throws Exception {

		return this.getWebElement().getSize();
	}

	public Point getLocaDimension() throws Exception {

		return this.getWebElement().getLocation();
	}

	public boolean isElementVisible() {

		try {
			return this.getWebElement().isDisplayed();
		} catch (Exception e) {
			return false;
		}

	}
	
	public boolean isElementVisible(int timeout) {

		try {
			WebDriverWait wait = new WebDriverWait(DriverFactory.getInstance().getDriver(), timeout);
			wait.until(ExpectedConditions.presenceOfElementLocated(this.getByType()));
			return this.getWebElement().isDisplayed();
		} catch (Exception e) {
			return false;
		}

	}
	public void checkElementVisisbilityWithException(int timeout,String Message) throws Exception {

		try {
			WebDriverWait wait = new WebDriverWait(DriverFactory.getInstance().getDriver(), timeout);
			wait.until(ExpectedConditions.presenceOfElementLocated(this.getByType()));
			
		} catch (Exception e) {
			throw new Exception(Message);
		}

	}

	public boolean isElementAvailable() {

		try {
			this.getWebElement();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	
	


}
